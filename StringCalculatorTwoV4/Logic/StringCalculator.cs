﻿using System.Collections.Generic;

namespace StringCalculatorTwoV4
{
    public class StringCalculator : IStringCalculator
    {
        private ISeparator _separator;
        private IDelimiter _delimiter;
        private IExceptionHandler _handler;

        public StringCalculator() 
        {
            _separator = new Separator();
            _delimiter = new Delimiter();
            _handler = new ExceptionHandler();
        }

        public int Subtract(string numbers)
        {
            var difference = 0;

            if (string.IsNullOrEmpty(numbers))
            {
                return difference;
            }

            var separatorArray = _separator.GetSeparators(numbers);
            var delimiterArray = _delimiter.GetDelimiters(numbers, separatorArray);
            var numbersList = _delimiter.GetNumbers(numbers, delimiterArray);

            _handler.ThrowExceptionNumbersGreaterThousand(numbersList);

            difference = GetDifference(numbersList);

            return difference;
        }
        
        private int GetDifference(List<int> numbersList)
        {
            var difference = 0;

            foreach (var number in numbersList)
            {
                if (number > 0)
                {
                    difference += number * -1;
                }
                else
                {
                    difference += number;
                }
            }

            return difference;
        }
    }
}
