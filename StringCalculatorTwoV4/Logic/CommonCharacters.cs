﻿
namespace StringCalculatorTwoV4
{
    public class CommonCharacters : ICommonCharacters
    {
        private readonly char comma = ',';
        private readonly char newLine = '\n';
        private readonly char hash = '#';
        private readonly char leftSquare = '[';
        private readonly char rightSquare = ']';
        private readonly char lessThan = '<';
        private readonly char greaterThan = '>';
        private readonly string doubleLessThan = "<<";
        private readonly string doubleGreaterThan = ">>";

        public char Comma => this.comma;
        public char NewLine => this.newLine;
        public char Hash => this.hash;
        public char LeftSquare => this.leftSquare;
        public char RightSquare => this.rightSquare;
        public char LessThan => this.lessThan;
        public char GreaterThan => this.greaterThan;
        public string DoubleLessThan => this.doubleLessThan;
        public string DoubleGreaterThan => this.doubleGreaterThan;
    }
}
