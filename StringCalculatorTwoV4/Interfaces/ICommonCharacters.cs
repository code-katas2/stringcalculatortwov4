﻿namespace StringCalculatorTwoV4
{
    public interface ICommonCharacters
    {
        char Comma { get; } 
        string DoubleGreaterThan { get; }
        string DoubleLessThan { get; }
        char GreaterThan { get; }
        char Hash { get; }
        char LeftSquare { get; }
        char LessThan { get; }
        char NewLine { get; }
        char RightSquare { get; }
    }
}