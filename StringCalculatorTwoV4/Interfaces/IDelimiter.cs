﻿using System.Collections.Generic;

namespace StringCalculatorTwoV4
{
    public interface IDelimiter
    {
        string[] GetDelimiters(string numbers, string[] separatorArray);
        List<int> GetNumbers(string numbers, string[] delimiterArray);
       
    }
}