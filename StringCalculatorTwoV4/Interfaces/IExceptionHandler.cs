﻿using System.Collections.Generic;

namespace StringCalculatorTwoV4
{
    public interface IExceptionHandler
    {
        void ThrowExceptionNumbersGreaterThousand(List<int> numbersList);
    }
}