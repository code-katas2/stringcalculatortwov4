﻿namespace StringCalculatorTwoV4
{
    public interface ISeparator
    {
        string[] GetSeparators(string numbers);
    }
}